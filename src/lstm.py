from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Embedding, Flatten, Dense
from keras.layers import LSTM
from tqdm import tqdm

import numpy as np
import pandas as pd # data processing
import matplotlib.pyplot as plt
import math

max_words = 10000
max_features = 10000
maxlen = 500
batch_size = 32
embeddings = True
glove_path = "../input/embeddings/glove.840B.300d/glove.840B.300d.txt";
max_emb = 10000
embedding_dim = 300



# 0 < size < 1
def load_data(size):
    train = pd.read_csv("../input/train.csv")
    return train_test_split(train, test_size=size), train

def tokenize_panda(panda_frame, max_words=max_words, max_sentences=maxlen):
    words = []
    print("  Adding words..")
    for i in range(min(panda_frame.shape[0], max_sentences)):
        words.append(panda_frame.iloc[i, 1])

    tokenizer = Tokenizer(num_words=max_words)
    print("  Fitting words to tokenizer..")
    tokenizer.fit_on_texts(words)

    print("  Extracting sequences...")
    sequences = tokenizer.texts_to_sequences(words)
    return tokenizer, sequences

def f1(y_true, y_pred):
    '''
    metric from here
    https://stackoverflow.com/questions/43547402/how-to-calculate-f1-macro-in-keras
    '''
    def recall(y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision
    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

######
# Convert values to embeddings
def text_to_array(text, embedding_dim=embedding_dim): # normalize and pads?
    empyt_emb = np.zeros(embedding_dim) # 300 dim of emb
    text = text[:-1].split()[:30]
    embeds = [embeddings_index.get(x, empyt_emb) for x in text]
    embeds+= [empyt_emb] * (30 - len(embeds))
    return np.array(embeds)

def batch_gen(data, shuffle=True, batch_size=128):
    n_batches = math.ceil(len(data) / batch_size)
    while True:
        if shuffle:
            data = data.sample(frac=1.)  # Shuffle the data.

        for i in range(n_batches):
            texts = data.iloc[i*batch_size:(i+1)*batch_size, 1]
            text_arr = np.array([text_to_array(text) for text in texts])
            targets = np.array(data["target"][i*batch_size:(i+1)*batch_size])
            yield (text_arr, targets)
######

print("Loading training dataset...")
(input_train, input_val), train = load_data(0.2)
insincere = train.loc[train['target'] == 1]
sincere = train.loc[train['target'] == 0]

print("Tokenizing dataset...")
tokenizer, sequences = tokenize_panda(train)
print("Tokenizing sincere...")
sincere_tokenizer, _ = tokenize_panda(sincere)
print("Tokenizing insincere...")
insincere_tokenizer, _ = tokenize_panda(insincere)

words_not_in_sincere = set(tokenizer.word_docs.keys()) - set(sincere_tokenizer.word_docs.keys())
words_not_in_insincere = set(tokenizer.word_docs.keys()) - set(insincere_tokenizer.word_docs.keys())


print("Found %s unique tokens." % len(tokenizer.word_index))
print("Found %s unique tokens used in sincere questions." % len(sincere_tokenizer.word_index))
print("Found %s unique tokens used in insincere questions." % len(insincere_tokenizer.word_index))

print("Tokens not in sincere:", len(words_not_in_sincere))
print("Tokens not in insincere:", len(words_not_in_insincere))
# Theese are not used yet..... but I'll leave it for now

word_index = tokenizer.word_index
sequences = pad_sequences(sequences, maxlen=maxlen)

print("Shape of data tensor:", sequences.shape)

if embeddings:
    print("Loading embeddings...")
    embeddings_index = {}
    with open(glove_path, "r") as f:
        i = 0
        for line in tqdm(f):
            if (i == max_emb):
                break

            values = line.split(" ")
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
            i += 1

    print('Found %s word vectors.' % len(embeddings_index))

    print("Preparing embeddings...")
    embedding_matrix = np.zeros((max_words, embedding_dim))
    for word, i in word_index.items():
        if i < max_words:
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector

    print("Modeling embeddings...")
    model = Sequential()
    #model.add(Embedding(max_features, embedding_dim, weights=[embedding_matrix],
    #                    trainable=False))
    model.add(LSTM(32, return_sequences=True, input_shape=(30, 300)))
    model.add(LSTM(32))
    model.add(Dense(1, activation="sigmoid"))
    model.summary()


    print("Training and evaluation")
    model.compile(optimizer="rmsprop",
                  loss='binary_crossentropy',
                  metrics=[f1])
    #####
    val_vects = np.array([text_to_array(X_text) for X_text in tqdm(input_val["question_text"][:3000])])
    val_y = np.array(input_val["target"][:3000])

    train_gen = batch_gen(input_train)
    val_gen = batch_gen(input_val)
    print(val_y[0])
    print(val_vects[0])
    history = model.fit_generator(train_gen, epochs=10, steps_per_epoch=1000,
                                  validation_data=(val_vects, val_y))
    ###


########
# prediction part
batch_size = 256
def batch_gen(test_df):
    n_batches = math.ceil(len(test_df) / batch_size)
    for i in range(n_batches):
        texts = test_df.iloc[i*batch_size:(i+1)*batch_size, 1]
        text_arr = np.array([text_to_array(text) for text in texts])
        yield text_arr

test_df = pd.read_csv("../input/test.csv")

all_preds = []
for x in tqdm(batch_gen(test_df)):
    all_preds.extend(model.predict(x).flatten())


y_te = (np.array(all_preds) > 0.5).astype(np.int)

submit_df = pd.DataFrame({"qid": test_df["qid"], "prediction": y_te})
submit_df.to_csv("submission.csv", index=False)
#########

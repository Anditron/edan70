import basics
import subprocess
import os
from flask import Flask, render_template, request
from flask_restful import Resource, Api

treshold = 0.33


# Create the application instance
app = Flask(__name__, template_folder="templates")

api = Api(app)
model = None

# Create a URL route in our application for "/"
@app.route('/')
def home():
    """
    This function just responds to the browser ULR
    localhost:5000/

    :return:        the rendered template 'home.html'
    """
    return render_template('home.html')


@app.route('/predict', methods=['POST'])
def prediction():
    text = request.form['insincerity']
    pred = model.predict(text)[0]
    if (pred > treshold):
        sincere='No'
    else:
        sincere='Yes'
    print(text, pred, sincere)
    #return "Sentence: {}<br />Prediction: {}".format(text, pred)
    return render_template('test.html', text=text, sincere=sincere, pred=pred)


# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    model = basics.InsincereClassifier()
    model.load()
    app.run(debug=True)

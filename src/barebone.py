import keras
from keras.layers import Dense
from keras.layers import LSTM, Embedding
import numpy as np
from keras import Sequential

model = Sequential()
model.add(Embedding(1000, 150, mask_zero=True))
model.add(LSTM(100))
model.add(Dense(2, activation="sigmoid"))
model.compile(loss="binary_crossentropy", optimizer="adam")
model.summary()

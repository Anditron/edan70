import os
import sys
import time
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import math
import pickle
import theano
from sklearn.model_selection import train_test_split
from sklearn import metrics

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, LSTM, Embedding, Dropout, Activation, Conv1D
from keras.layers import Bidirectional, GlobalMaxPool1D, GlobalMaxPooling1D, GlobalAveragePooling1D
from keras.layers import Input, Embedding, Dense, Conv2D, MaxPool2D, concatenate
from keras.layers import Reshape, Flatten, Concatenate, Dropout, SpatialDropout1D
from keras.optimizers import Adam
from keras.models import Model
from keras import backend as K
from keras.engine.topology import Layer
from keras import initializers, regularizers, constraints, optimizers, layers
import tensorflow as tf

## some config values
embed_size = 300      # how big is each word vector
max_features = 900    # how many unique words to use (i.e num rows in embedding
                      # vector) (this might need to be changed when using
                      # lightweight version)
maxlen = 70           # max number of words in a question to use
max_sentences = 3000  # max number of sentences to load from test and train data
max_embeddings = 3000 # max number of lines to load from embeddings
random_seed = 2018
tokenizer = Tokenizer(num_words=max_features)
test = []

class Attention(Layer):
    def __init__(self, step_dim,
                 W_regularizer=None, b_regularizer=None,
                 W_constraint=None, b_constraint=None,
                 bias=True, **kwargs):
        self.supports_masking = True
        self.init = initializers.get('glorot_uniform')

        self.W_regularizer = regularizers.get(W_regularizer)
        self.b_regularizer = regularizers.get(b_regularizer)

        self.W_constraint = constraints.get(W_constraint)
        self.b_constraint = constraints.get(b_constraint)

        self.bias = bias
        self.step_dim = step_dim
        self.features_dim = 0
        super(Attention, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) == 3

        self.W = self.add_weight((input_shape[-1],),
                                 initializer=self.init,
                                 name='{}_W'.format(self.name),
                                 regularizer=self.W_regularizer,
                                 constraint=self.W_constraint)
        self.features_dim = input_shape[-1]

        if self.bias:
            self.b = self.add_weight((input_shape[1],),
                                     initializer='zero',
                                     name='{}_b'.format(self.name),
                                     regularizer=self.b_regularizer,
                                     constraint=self.b_constraint)
        else:
            self.b = None

        self.built = True

    def compute_mask(self, input, input_mask=None):
        return None

    def call(self, x, mask=None):
        features_dim = self.features_dim
        step_dim = self.step_dim

        eij = K.reshape(K.dot(K.reshape(x, (-1, features_dim)),
                        K.reshape(self.W, (features_dim, 1))), (-1, step_dim))

        if self.bias:
            eij += self.b

        eij = K.tanh(eij)

        a = K.exp(eij)

        if mask is not None:
            a *= K.cast(mask, K.floatx())

        a /= K.cast(K.sum(a, axis=1, keepdims=True) + K.epsilon(), K.floatx())

        a = K.expand_dims(a)
        weighted_input = x * a
        self.a = a
        sum_of_K =  K.sum(weighted_input, axis=1)

        return [sum_of_K, a]


    def compute_output_shape(self, input_shape):
        return [(input_shape[0],  self.features_dim), (self.a.shape,
                                                       self.features_dim)]



class InsincereClassifier():

    def __init__(self, model=None, embeddings=None, tokenizer=None,
                 embed_size=embed_size, max_features=max_features,
                 maxlen=maxlen, max_sentences=max_sentences,
                 max_embeddings=max_embeddings, random_seed=random_seed):
        self.model = model
        self.embeddings = embeddings
        self.tokenizer = tokenizer
        self.embed_size = embed_size
        self.max_features = max_features
        self.maxlen = maxlen
        self.max_sentences = max_sentences
        self.max_embeddings = max_embeddings
        self.random_seed = random_seed

    def load_and_prec(self):
        print("Loading train and test data...")
        train_df = pd.read_csv("../input/train.csv")[:self.max_sentences]
        test_df = pd.read_csv("../input/test.csv")[:self.max_sentences]
        print("Train shape : ",train_df.shape)
        print("Test shape : ",test_df.shape)

        ## split to train and val
        train_df, val_df = train_test_split(train_df, test_size=0.1,
                                            random_state=self.random_seed)


        ## fill up the missing values
        train_X = train_df["question_text"].fillna("_##_").values
        val_X = val_df["question_text"].fillna("_##_").values
        test_X = test_df["question_text"].fillna("_##_").values

        ## Tokenize the sentences
        tokenizer.fit_on_texts(list(train_X))
        train_X = tokenizer.texts_to_sequences(train_X)
        val_X = tokenizer.texts_to_sequences(val_X)
        test_X = tokenizer.texts_to_sequences(test_X)

        ## Pad the sentences
        train_X = pad_sequences(train_X, maxlen=self.maxlen)
        val_X = pad_sequences(val_X, maxlen=self.maxlen)
        test_X = pad_sequences(test_X, maxlen=self.maxlen)

        ## Get the target values
        train_y = train_df['target'].values
        val_y = val_df['target'].values

        #shuffling the data
        np.random.seed(self.random_seed)
        trn_idx = np.random.permutation(len(train_X))
        val_idx = np.random.permutation(len(val_X))

        train_X = train_X[trn_idx]
        val_X = val_X[val_idx]
        train_y = train_y[trn_idx]
        val_y = val_y[val_idx]

        self.tokenizer = tokenizer
        pos = 0
        for entry in train_y:
            if entry == 1:
                pos += 1

        print(pos)
        print(len(train_y) - pos)

        return train_X, val_X, test_X, train_y, val_y, tokenizer.word_index

    def load_glove(self, word_index):
        print("Loading GloVe embeddings...")
        EMBEDDING_FILE = '../input/embeddings/glove.840B.300d/glove.840B.300d.txt'
        def get_coefs(word,*arr): return word, np.asarray(arr, dtype='float32')
        embeddings_index = {}
        with open(EMBEDDING_FILE) as f:
            i = 0
            for o in f:
                word, weights = get_coefs(*o.split(" "))
                embeddings_index[word] = weights
                i += 1
                if i == self.max_embeddings:
                    break

        all_embs = np.stack(embeddings_index.values())
        emb_mean,emb_std = -0.005838499,0.48782197
        self.embed_size = all_embs.shape[1]

        # word_index = tokenizer.word_index
        nb_words = min(self.max_features, len(word_index))
        embedding_matrix = np.random.normal(emb_mean, emb_std, (nb_words, self.embed_size))
        for word, i in word_index.items():
            if i >= self.max_features: continue
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None: embedding_matrix[i] = embedding_vector

        self.embeddings = embedding_matrix

        return embedding_matrix


    def model_lstm_atten(self, embedding_matrix, return_probability=False):
        print("Building model...")
        inp = Input(shape=(self.maxlen,))
        x = Embedding(self.max_features, self.embed_size, weights=[embedding_matrix], trainable=False)(inp)
        x = Bidirectional(LSTM(128, return_sequences=True))(x)
        x = Bidirectional(LSTM(64, return_sequences=True))(x)
        attention, a = Attention(self.maxlen)(x)
        x = Dense(64, activation="relu")(attention)
        x = Dense(1, activation="sigmoid")(x)
        model = Model(inputs=inp, outputs=x)
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        self.model = model
        self.attention = attention

        return model

    def train_pred(self, model, data, epochs=2):
        print("Training model...")
        train_X, val_X, test_X, train_y, val_y, _ = data
        for e in range(epochs):
            model.fit(train_X, train_y, batch_size=512, epochs=1, validation_data=(val_X, val_y))
            print("Predicting validation score of the model...")
            pred_val_y = model.predict([val_X], batch_size=1024, verbose=0) # instead of predicting validation y each step only do it after last epoch.

        model.save_weights('model.h5')
        print("Predicting test data...")
        pred_test_y = model.predict([test_X], batch_size=1024, verbose=0)
        return pred_val_y, pred_test_y


    def run_train_and_pred(self):
        train_X, val_X, test_X, train_y, val_y, word_index = self.load_and_prec()
        vocab = []
        for w,k in word_index.items():
            vocab.append(w)
            if k >= self.max_features:
                break
        embedding_matrix = self.load_glove(word_index)

        data = (train_X, val_X, test_X, train_y, val_y, word_index)

        pred_val_y, pred_test_y = self.train_pred(self.model_lstm_atten(embedding_matrix), data, epochs = 3)

        pred_test_y = (pred_test_y > 0.33).astype(int)
        test_df = pd.read_csv("../input/test.csv", usecols=["qid"])[:self.max_sentences]
        out_df = pd.DataFrame({"qid":test_df["qid"].values})
        out_df['prediction'] = pred_test_y
        out_df.to_csv("submission.csv", index=False)
        print("done.")


    #region IO load and save
    def load_model(self, weights_path, embedding_matrix):
        model = self.model_lstm_atten(embedding_matrix, False)
        model.load_weights(weights_path)
        return model

    def load_embeddings(self, emb_matrix_path):
        return pickle.load(open(emb_matrix_path, "rb"))

    def load_tokenizer(self, tokenizer_path):
        return pickle.load(open(tokenizer_path, "rb"))

    def load(self,
             model_path="../load/model.h5",
             embeddings_path="../load/embedding_matrix.p",
             tokenizer_path="../load/tokenizer.p"):
        self.embeddings = self.load_embeddings(embeddings_path)
        self.tokenizer = self.load_tokenizer(tokenizer_path)
        self.max_features = self.tokenizer.num_words
        self.model = self.load_model(model_path, self.embeddings)
        global graph
        self.graph = tf.get_default_graph()
    #endregion

    def save(self,
             model_path="../load/model.h5",
             embeddings_path="../load/embedding_matrix.p",
             tokenizer_path="../load/tokenizer.p"):
        self.model.save_weights(model_path) # TODO UPDATE
        pickle.dump(open(embeddings_path, "wb"))
        pickle.dump(open(tokenizer_path, "wb"))

    def predict(self, sentence):
        if not self.model or not self.tokenizer:
            print("Need to either load or run func run_train_and_pred first.")
            return -1
        sen = self.get_predictable_text(sentence)
        with self.graph.as_default():
            prediction = self.model.predict(sen)
            return prediction[0]

    def predict_attention(self, sentence, layer=4):
        model = self.model
        intermediate_layer_model = Model(inputs=model.input,
                                         outputs=model.layers[layer].output)
        sen = self.get_predictable_text(sentence)
        words = sentence.split()
        words = [x for x in words if self.tokenizer.word_index.get(x)]
        length = len(words)
        attention = intermediate_layer_model.predict(sen)[1][0]

        return attention[-length:], words, self.predict(sentence)


    def get_predictable_text(self, sentence):
        sen = [sentence]
        sen = self.tokenizer.texts_to_sequences(sen)
        sen = pad_sequences(sen, maxlen=self.maxlen)
        return sen


def predict_once(sen):
    ic = InsincereClassifier()
    ic.load()
    pred = ic.predict(sen)
    K.clear_session()
    return pred


if __name__ == '__main__':
    if len(sys.argv) > 1 :
        sen = " ".join(sys.argv[1:])
        sen = sen.lower()
        print("Using sentence:", sen)

        ic = InsincereClassifier()
        ic.load()
        print(sen, ":", ic.predict(sen))
    else:
        ic = InsincereClassifier()
        ic.run_train_and_pred()

# Insincere questions classification
This project is intended both as a project for the LTH course EDAN90 and as an entry for the compettion
[Quora Insincere Questions
Classification](https://www.kaggle.com/c/quora-insincere-questions-classification). 


# How to setup
The dataset is available at [the page of the competition](https://www.kaggle.com/c/quora-insincere-questions-classification/data).
It is rather big (6Gb ziped) with the embeddings included. Therefore only the
csv files, without the embeddings are uploaded.

If one wants to use the kaggle api it can be downloaded using

```
kaggle competitions download -c quora-insincere-questions-classification
```

> Note: Need an account to download the dataset.

The embedding needs to be in the folder input/embeddings. 


Scikit, Pandas, flask, and tqdm are also needed. Anaconda includes Scikit and Pandas and many more modules, so it is recommended to use it. 

tqdm and flask can be installed from pip. flask is needed to be able to run the server, 

```
pip install tqdm
pip install flask
```

Anaconda can be found [here](https://www.anaconda.com/).

# How to run
To run the server using the model in folder load, just run

```
python src/server.py
```

This will load the weights from the model and the tokenizer from folder load and setup the server.

The server will be available at 127.0.0.1:5000.